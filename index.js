// s18 a1

// Trainer
function Trainer(name, age, pokemon, friends){
	// Properties
	this.name = name;
	this.age = age;
	this.pokemon = pokemon;
	this.friends = friends;
	// Methods
	this.talk = function(chosenPokemon){
		console.log(chosenPokemon + "! I choose you!");
	}
}

let cams = new Trainer(
		"Cams Perez", 
		26, 
		['Pikachu','Bulbasaur','Charizard','Squirtle'], 
		{
			hoenn: ['Max','May'],
			kanto: ['Brock','Misty'],
		}
	)
console.log(cams);

console.log("Result from dot notation:"); 
console.log(cams.name);

console.log("Result of square bracket notation:");
console.log(cams['pokemon']);

console.log("Result of talk method:");
console.log(cams.talk('Pikachu'));


// Pokemon
function Pokemon(name, level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	// Methods
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		newHealth = (target.health - this.attack);
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));
		if (newHealth <= 0) {
			target.faint();
		}
	}
	this.faint = function(){
		console.log(this.name + ' fainted');
	}
}

 
let pikachu = new Pokemon("Pikachu", 22);
let bulbasaur = new Pokemon("Bulbasaur", 16);
let charizard = new Pokemon("Charizard", 36);
let squirtle = new Pokemon("Squirtle", 16);
let charmander = new Pokemon("Charmander", 16);
let caterpie = new Pokemon("Caterpie", 7);
let rattata = new Pokemon('Rattata', 8);

console.log(pikachu);
console.log(caterpie);
console.log(charmander);
console.log(rattata);

pikachu.tackle(caterpie);
pikachu.tackle(charmander);
pikachu.tackle(rattata);